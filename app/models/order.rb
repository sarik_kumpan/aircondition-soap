class Order < ApplicationRecord
end

class OrderData < WashOut::Type
    map :order_imformation => [{ :code => :string, :status => :string, :reciver => :string, :destination => :string, :weight => :float }]
end