class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :sender
      t.string :reciver
      t.string :destination
      t.float :weight
      t.string :code
      t.string :status
      t.timestamps
    end
  end
end
