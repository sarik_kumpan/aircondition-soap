require 'order'
class ServiceController < ApplicationController
  soap_service namespace: 'urn:WashOut'

  soap_action "update_info",
              :args =>  { :no => :string , :temperature => :string, :humid => :string, :date_t => :string },
              :return => :string
  def update_info
   if(Aircondition.create!( :no => params[:no],:temperature => params[:temperature],:humid => params[:humid],:date_t => params[:date_t]))
    render :soap =>  "add complete"
   else
    render :soap => "add fail"
   end
  end

  soap_action "get_info",
              :return => [{:aircondition_info => [ {
                            :no => :integer,
                            :temp => :float,
                            :humid => :float,
                            :date => :date
              } ] } ]
  def get_info
   @info = Aircondition.all
   array_info = []
   @info.each do |info|
    hsh = {}
    hsh["no"] = info.no
    hsh["temp"] = info.temperature
    hsh["humid"] = info.humid
    hsh["date"] = info.date_t
    array_info.push(hsh)
   end
   render :soap =>  [{:aircondition_info => array_info}]
  end  

  soap_action "you_info",
              :return => {:my_info => {
                            :name => :string,
                            :number => :integer,
                            :hobbie => { :firsts => :string,
                                        :second => :string
                                        }
                            }}
  def you_info
    @hobbie = {}
    @hobbie["firsts"] = "anime"
    @hobbie["second"] = "run"
    @info = {}
    @info[:name] = "sarik kumpan"
    @info[:number] = 5801012610164
    @info[:hobbie] = @hobbie
    render :soap => { :my_info => @info}
   end  

  soap_action "send_package",
              :args =>  {:code =>:string, :sender => :string , :reciver => :string, :destination => :string, :weight => :string },
              :return => :string
  def send_package
    if(Order.create!( :code =>   params[:code],:status => "store",:sender => params[:sender],:reciver => params[:reciver],:destination => params[:destination],:weight => params[:weight]))
      render :soap => params[:code]
    else
      render :soap => "try again"
    end
  end

  soap_action "get_info_package",
              :args => nil,
              :return => [OrderData]
  def get_info_package
      array_of_package = []
      Order.all.find_each do |package|
          hsh = {}
          hsh["code"] = package.code
          hsh["status"] = package.status
          hsh["reciver"] = package.reciver
          hsh["destination"] = package.destination
          hsh["weight"] = package.weight
          array_of_package.push(hsh)
      end
      render :soap => [ :order_imformation => array_of_package ] 
  end

  soap_action "get_info_package_non_finish",
              :args => nil,
              :return => [OrderData]
  def get_info_package_non_finish
      array_of_package = []
      Order.where(status: "store").find_each do |package|
          hsh = {}
          hsh["code"] = package.code
          hsh["status"] = package.status
          hsh["reciver"] = package.reciver
          hsh["destination"] = package.destination
          hsh["weight"] = package.weight
          array_of_package.push(hsh)
      end
      render :soap => [:order_imformation => array_of_package] 
  end

  soap_action "update_status",
              :args   => { :code => :string , :status => :string },
              :return => :string
  def update_status
    if params[:code].nil?
        raise SOAPError, "Code Package is empty"
    else
        package = Order.find_by code: params[:code]
        if package.nil?
            raise SOAPError, "not found package"
        else
            package.status = params[:status]
            package.save
        end
    end
    render :soap => "successfully update status package."
  end

end
