class CreateAirconditions < ActiveRecord::Migration[5.2]
  def change
    create_table :airconditions do |t|
      t.integer :no
      t.float :temperature
      t.float :humid
      t.date :date_t
      t.timestamps
    end
  end
end
